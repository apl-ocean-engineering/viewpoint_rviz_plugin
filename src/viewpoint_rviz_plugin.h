// Based on pose_array_display.cpp
// TODO: Proper license/copyright statement at top of this file ...

#pragma once

#include "inspection_planner_msgs/ViewpointList.h"

#include <rviz/message_filter_display.h>

#include <boost/ptr_container/ptr_vector.hpp>

namespace Ogre
{
class ManualObject;
}

namespace rviz
{
class EnumProperty;
class ColorProperty;
class FloatProperty;
class Arrow;
class Axes;
}

namespace viewpoint_rviz_plugin {

/** @brief Displays an inspection_planner_msgs/ViewpointList message as a bunch of line-drawn arrows. */
class ViewpointDisplay : public rviz::MessageFilterDisplay<inspection_planner_msgs::ViewpointList>
{
  Q_OBJECT
public:
  ViewpointDisplay();
  ~ViewpointDisplay() override;

protected:
  void onInitialize() override;
  void reset() override;
  void processMessage(const inspection_planner_msgs::ViewpointList::ConstPtr& msg) override;

private:
  struct OgreViewpoint
  {
    Ogre::Vector3 position;
    Ogre::Quaternion orientation;
    float cost;
    float reward;
  };

  bool setTransform(std_msgs::Header const& header);
  // Handle logic for hiding/showing different options in the UI based on 
  // the marker type and color source.
  void updateProperties();

  // Update the 2D arrows (position, color, length)
  void updateArrows2d();
  // Update the 3D arrows (position, orientation, color, but NOT geometry)
  void updateArrows3d();
  // Update the Axes (position and orientation, but NOT geometry)
  void updateAxes();
  // Call the appropriate function of: updateArrows2d, updateArrows3d, updateAxes
  void updateDisplay();
  
  // Helper function to contruct a new rviz::Axes and attach it to the scene manager
  // TODO: Switch over to smart pointers?
  rviz::Axes* makeAxes(); 
  // Helper function to contruct a new rviz::Arrow and attach it to the scene manager
  rviz::Arrow* makeArrow3d();
  
  // Return the color to use for specified viewpoint, based on the current UI selections.
  Ogre::ColourValue getColor(const OgreViewpoint& viewpoint) const;  
  // Return min and max value of the viewpoints' costs. Does not modify input if 
  // viewpoints is empty.
  void getCostBounds(float* min_cost, float* max_cost) const;
  void getRewardBounds(float* min_reward, float* max_reward) const;

  std::vector<OgreViewpoint> viewpoints_;
  boost::ptr_vector<rviz::Arrow> arrows3d_;
  boost::ptr_vector<rviz::Axes> axes_;

  Ogre::SceneNode* arrow_node_;
  Ogre::SceneNode* axes_node_;
  Ogre::ManualObject* manual_object_;

  rviz::EnumProperty* shape_property_;
  rviz::EnumProperty* color_source_property_;
  rviz::ColorProperty* arrow_color_property_;
  rviz::FloatProperty* arrow_alpha_property_;

  rviz::FloatProperty* arrow2d_length_property_;

  rviz::FloatProperty* arrow3d_head_radius_property_;
  rviz::FloatProperty* arrow3d_head_length_property_;
  rviz::FloatProperty* arrow3d_shaft_radius_property_;
  rviz::FloatProperty* arrow3d_shaft_length_property_;

  rviz::FloatProperty* axes_length_property_;
  rviz::FloatProperty* axes_radius_property_;

  rviz::FloatProperty* color_min_bound_;
  rviz::FloatProperty* color_max_bound_;

  // TODO: add "Use Rainbow" checkbox, and when unchecked have it revewl
  //     two new color pickers, allowing the user to pick min/max values,
  //     then have color linearly interpolate between them?
  //     (Really, I just want an alternative to the objectively terrible
  //     rainbow colormap...)

private Q_SLOTS:
  /// Update the interface and visible shapes based on the selected shape type.
  void updateShapeChoice();

  /// Update whether the pose arrows are colored based on cost, reward, or nothing.
  /// Also resets the colormap bounds.
  void updateColorSource();

  /// Update bounds for color scale (triggers full update)
  void updateColorBounds();

  /// Update the arrow color.
  void updateArrowColor();

  /// Update the flat arrow geometry.
  void updateArrow2dGeometry();

  /// Update the 3D arrow geometry.
  void updateArrow3dGeometry();

  /// Update the axes geometry.
  void updateAxesGeometry();
};  // class ViewpointDisplay

} // namespace viewpoint_rviz_plugin

