#include <OgreManualObject.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>

#include <rviz/display_context.h>
#include <rviz/frame_manager.h>
#include <rviz/properties/enum_property.h>
#include <rviz/properties/color_property.h>
#include <rviz/properties/float_property.h>
#include <rviz/ogre_helpers/arrow.h>
#include <rviz/ogre_helpers/axes.h>

#include "viewpoint_rviz_plugin.h"

namespace rviz
{
namespace
{
struct ShapeType
{
  enum
  {
    Arrow2d,
    Arrow3d,
    Axes,
  };
};

struct ColorSource {
  enum {
    Solid,
    Cost,
    Reward,
  };
};

} // namespace

// Taken directly from point_cloud_transformers.cpp
// Blue is low, red is high.
static void getRainbowColor(float value, Ogre::ColourValue& color)
{
  // this is HSV color palette with hue values going only from 0.0 to 0.833333.

  value = std::min(value, 1.0f);
  value = std::max(value, 0.0f);

  float h = value * 5.0f + 1.0f;
  int i = floor(h);
  float f = h - i;
  if (!(i & 1))
    f = 1 - f; // if i is even
  float n = 1 - f;

  if (i <= 1)
    color[0] = n, color[1] = 0, color[2] = 1;
  else if (i == 2)
    color[0] = 0, color[1] = n, color[2] = 1;
  else if (i == 3)
    color[0] = 0, color[1] = 1, color[2] = n;
  else if (i == 4)
    color[0] = n, color[1] = 1, color[2] = 0;
  else if (i >= 5)
    color[0] = 1, color[1] = n, color[2] = 0;
}


} // namespace rviz

namespace viewpoint_rviz_plugin {

ViewpointDisplay::ViewpointDisplay() : manual_object_(nullptr)
{
  shape_property_ = new rviz::EnumProperty("Shape", "Arrow (Flat)", 
		                           "Shape to display the pose as.",
                                           this, SLOT(updateShapeChoice()));

  color_source_property_ = new rviz::EnumProperty("Color Source", "Solid",
		                                  "How to determine color of arrows",
						  this, SLOT(updateColorSource()));
  // TODO: add additional min/max colors? a key? Allow choice of colormap?

  arrow_color_property_ = new rviz::ColorProperty("Color", QColor(255, 25, 0), 
		                                  "Color to draw the arrows.",
                                                  this, SLOT(updateArrowColor()));

  auto alpha_help = "Amount of transparency to apply to the displayed poses.";
  arrow_alpha_property_ = new rviz::FloatProperty("Alpha", 1, alpha_help, this,
                                                  SLOT(updateArrowColor()));

  arrow2d_length_property_ = new rviz::FloatProperty("Arrow Length", 0.3, 
		                                     "Length of the arrows.", 
						     this,
                                                     SLOT(updateArrow2dGeometry()));

  arrow3d_head_radius_property_ =
      new rviz::FloatProperty("Head Radius", 0.03, "Radius of the arrow's head, in meters.", this,
                        SLOT(updateArrow3dGeometry()));

  arrow3d_head_length_property_ =
      new rviz::FloatProperty("Head Length", 0.07, "Length of the arrow's head, in meters.", this,
                        SLOT(updateArrow3dGeometry()));

  arrow3d_shaft_radius_property_ =
      new rviz::FloatProperty("Shaft Radius", 0.01, "Radius of the arrow's shaft, in meters.", this,
                        SLOT(updateArrow3dGeometry()));

  arrow3d_shaft_length_property_ =
      new rviz::FloatProperty("Shaft Length", 0.23, "Length of the arrow's shaft, in meters.", this,
                        SLOT(updateArrow3dGeometry()));

  axes_length_property_ = new rviz::FloatProperty("Axes Length", 0.3, "Length of each axis, in meters.", this,
                                            SLOT(updateAxesGeometry()));

  axes_radius_property_ = new rviz::FloatProperty("Axes Radius", 0.01, "Radius of each axis, in meters.", this,
                                            SLOT(updateAxesGeometry()));

  color_min_bound_ = new rviz::FloatProperty("Min Value", 0.0,
		                             "Minimum bound for colorscale.",
					     this,
					     SLOT(updateColorBounds()));
  color_max_bound_ = new rviz::FloatProperty("Max Value", 100.0,
		                             "Maximum bound for colorscale.",
					     this,
					     SLOT(updateColorBounds()));

  shape_property_->addOption("Arrow (Flat)", rviz::ShapeType::Arrow2d);
  shape_property_->addOption("Arrow (3D)", rviz::ShapeType::Arrow3d);
  shape_property_->addOption("Axes", rviz::ShapeType::Axes);
  color_source_property_->addOption("Solid", rviz::ColorSource::Solid);
  color_source_property_->addOption("Cost", rviz::ColorSource::Cost);
  color_source_property_->addOption("Reward", rviz::ColorSource::Reward);
  arrow_alpha_property_->setMin(0);
  arrow_alpha_property_->setMax(1);
}

ViewpointDisplay::~ViewpointDisplay() {
  if (initialized())
  {
    scene_manager_->destroyManualObject(manual_object_);
  }
}

void ViewpointDisplay::onInitialize() {
  MFDClass::onInitialize();
  manual_object_ = scene_manager_->createManualObject();
  manual_object_->setDynamic(true);
  scene_node_->attachObject(manual_object_);
  arrow_node_ = scene_node_->createChildSceneNode();
  axes_node_ = scene_node_->createChildSceneNode();
  updateShapeChoice();
}

void ViewpointDisplay::processMessage(const inspection_planner_msgs::ViewpointList::ConstPtr& msg) {
  if (!setTransform(msg->header)) {
    setStatus(rviz::StatusProperty::Error, "Topic", "Failed to look up transform");
    return;
  }

  viewpoints_.resize(msg->viewpoints.size());
  for (std::size_t i = 0; i < msg->viewpoints.size(); ++i) {
    viewpoints_[i].position = Ogre::Vector3(msg->viewpoints[i].x, 
		                            msg->viewpoints[i].y,
				            msg->viewpoints[i].z);

    tf2::Quaternion quat;
    quat.setRPY(0, 0, msg->viewpoints[i].yaw);
    viewpoints_[i].orientation = Ogre::Quaternion(quat.w(), quat.x(), quat.y(), quat.z());
    viewpoints_[i].cost = msg->viewpoints[i].cost;
    viewpoints_[i].reward = msg->viewpoints[i].reward;
  }

  updateDisplay();
  context_->queueRender();
}

bool ViewpointDisplay::setTransform(std_msgs::Header const& header) {
  Ogre::Vector3 position;
  Ogre::Quaternion orientation;
  if (!context_->getFrameManager()->getTransform(header, position, orientation))
  {
    ROS_ERROR("Error transforming pose '%s' from frame '%s' to frame '%s'", qPrintable(getName()),
              header.frame_id.c_str(), qPrintable(fixed_frame_));
    return false;
  }
  scene_node_->setPosition(position);
  scene_node_->setOrientation(orientation);
  return true;
}

void ViewpointDisplay::updateArrows2d() {
  manual_object_->clear();

  float length = arrow2d_length_property_->getFloat();
  size_t num_poses = viewpoints_.size();
  manual_object_->estimateVertexCount(num_poses * 6);
  manual_object_->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST,
                        Ogre::ResourceGroupManager::INTERNAL_RESOURCE_GROUP_NAME);
  for (size_t i = 0; i < num_poses; ++i) {
    const Ogre::Vector3& pos = viewpoints_[i].position;
    const Ogre::Quaternion& orient = viewpoints_[i].orientation;
    Ogre::ColourValue color = getColor(viewpoints_[i]);
    Ogre::Vector3 vertices[6];
    vertices[0] = pos;                                        // back of arrow
    vertices[1] = pos + orient * Ogre::Vector3(length, 0, 0); // tip of arrow
    vertices[2] = vertices[1];
    vertices[3] = pos + orient * Ogre::Vector3(0.75 * length, 0.2 * length, 0);
    vertices[4] = vertices[1];
    vertices[5] = pos + orient * Ogre::Vector3(0.75 * length, -0.2 * length, 0);

    for (int i = 0; i < 6; ++i) {
      manual_object_->position(vertices[i]);
      manual_object_->colour(color);
    }
  }
  manual_object_->end();
}

void ViewpointDisplay::updateDisplay() {
  int shape = shape_property_->getOptionInt();
  switch (shape)
  {
  case rviz::ShapeType::Arrow2d:
    updateArrows2d();
    arrows3d_.clear();
    axes_.clear();
    break;
  case rviz::ShapeType::Arrow3d:
    updateArrows3d();
    manual_object_->clear();
    axes_.clear();
    break;
  case rviz::ShapeType::Axes:
    updateAxes();
    manual_object_->clear();
    arrows3d_.clear();
    break;
  }
}

void ViewpointDisplay::updateArrows3d() {
  while (arrows3d_.size() < viewpoints_.size()) {
    arrows3d_.push_back(makeArrow3d());
  }
  while (arrows3d_.size() > viewpoints_.size()) {
    arrows3d_.pop_back();
  }

  Ogre::Quaternion adjust_orientation(Ogre::Degree(-90), Ogre::Vector3::UNIT_Y);
  for (std::size_t i = 0; i < viewpoints_.size(); ++i) {
    arrows3d_[i].setPosition(viewpoints_[i].position);
    arrows3d_[i].setOrientation(viewpoints_[i].orientation * adjust_orientation);
    auto color = getColor(viewpoints_[i]);
    arrows3d_[i].setColor(color);
  }
}

void ViewpointDisplay::updateAxes() {
  while (axes_.size() < viewpoints_.size()) {
    axes_.push_back(makeAxes());
  }
  while (axes_.size() > viewpoints_.size()) {
    axes_.pop_back();
  }
  for (std::size_t i = 0; i < viewpoints_.size(); ++i) {
    axes_[i].setPosition(viewpoints_[i].position);
    axes_[i].setOrientation(viewpoints_[i].orientation);
    // TODO: Consider setting the color of axes as well?
  }
}

// This is a bit ugly (it queries the properties once per arrow, rather than once per input),
// but that wound up being easier given how distributed the plotting is.
// Rather than passing the color by reference, this just returns a new object, since that's
// the same API as rviz's getOgreColor()
Ogre::ColourValue ViewpointDisplay::getColor(const OgreViewpoint& viewpoint) const {

  int color_source = color_source_property_->getOptionInt();
  auto color = arrow_color_property_->getOgreColor();
  color.a = arrow_alpha_property_->getFloat();

  auto min_val = color_min_bound_->getFloat();
  auto max_val = color_max_bound_->getFloat();
  // TODO: Add options for different colormap?
  switch (color_source) {
    case rviz::ColorSource::Cost:
      {
      float scaled_cost = (viewpoint.cost - min_val) / (max_val - min_val);
      rviz::getRainbowColor(scaled_cost, color);
      }
      break;
    case rviz::ColorSource::Reward:
      {
      float scaled_reward = (viewpoint.reward - min_val) / (max_val - min_val);
      rviz::getRainbowColor(scaled_reward, color);
      }
      break;
    case rviz::ColorSource::Solid:
      // Already pulled color from properties...
      break;
    default:
      ROS_ERROR_STREAM("Unrecognized value for color_source: " << color_source);
  }
  return color;
}


rviz::Arrow* ViewpointDisplay::makeArrow3d() {
  rviz::Arrow* arrow =
      new rviz::Arrow(scene_manager_, arrow_node_, arrow3d_shaft_length_property_->getFloat(),
                arrow3d_shaft_radius_property_->getFloat(), arrow3d_head_length_property_->getFloat(),
                arrow3d_head_radius_property_->getFloat());
  return arrow;
}

rviz::Axes* ViewpointDisplay::makeAxes() {
  return new rviz::Axes(scene_manager_, axes_node_, axes_length_property_->getFloat(),
                  axes_radius_property_->getFloat());
}

void ViewpointDisplay::reset() {
  MFDClass::reset();
  if (manual_object_)
  {
    manual_object_->clear();
  }
  arrows3d_.clear();
  axes_.clear();
}

void ViewpointDisplay::getCostBounds(float* min_cost, float* max_cost) const {
  if (viewpoints_.empty()) {
    return;
  }
  *min_cost = std::numeric_limits<float>::max();
  *max_cost = std::numeric_limits<float>::min();
  for(const auto& viewpoint : viewpoints_) {
    *min_cost = std::min(*min_cost, viewpoint.cost);
    *max_cost = std::max(*max_cost, viewpoint.cost);
  }
}

void ViewpointDisplay::getRewardBounds(float* min_reward, float* max_reward) const {
  *min_reward = std::numeric_limits<float>::max();
  *max_reward = std::numeric_limits<float>::min();
  for(const auto& viewpoint : viewpoints_) {
    *min_reward = std::min(*min_reward, viewpoint.reward);
    *max_reward = std::max(*max_reward, viewpoint.reward);
  }
}


void ViewpointDisplay::updateColorSource() {
  float min_val = 0;
  float max_val = 100;
  int source = color_source_property_->getOptionInt();
  switch(source) {
    case rviz::ColorSource::Cost:
      getCostBounds(&min_val, &max_val);
      break;
    case rviz::ColorSource::Reward:
      getRewardBounds(&min_val, &max_val);
      break;
  }
  color_min_bound_->setFloat(min_val);
  color_max_bound_->setFloat(max_val);

  updateProperties();
  if (initialized()) {
    updateDisplay();
  }
}

void ViewpointDisplay::updateColorBounds() {
  if (initialized()) {
    updateDisplay();
  }
}

void ViewpointDisplay::updateProperties() {

  int shape = shape_property_->getOptionInt();
  bool use_arrow2d = shape == rviz::ShapeType::Arrow2d;
  bool use_arrow3d = shape == rviz::ShapeType::Arrow3d;
  bool use_arrow = use_arrow2d || use_arrow3d;
  bool use_axes = shape == rviz::ShapeType::Axes;

  int source = color_source_property_->getOptionInt();
  bool use_cost = source == rviz::ColorSource::Cost;
  bool use_reward = source == rviz::ColorSource::Reward;
  bool use_solid = source == rviz::ColorSource::Solid;

  // Only allow picking if using a solid color AND one of the arrows.
  arrow_color_property_->setHidden(!use_arrow || !use_solid);
  arrow_alpha_property_->setHidden(!use_arrow);

  // Axes don't support switching colors.
  color_source_property_->setHidden(!use_arrow);

  // Only show bounds if color is changing with cost/reward.
  color_min_bound_->setHidden(!use_arrow || use_solid);
  color_max_bound_->setHidden(!use_arrow || use_solid);

  arrow2d_length_property_->setHidden(!use_arrow2d);

  arrow3d_shaft_length_property_->setHidden(!use_arrow3d);
  arrow3d_shaft_radius_property_->setHidden(!use_arrow3d);
  arrow3d_head_length_property_->setHidden(!use_arrow3d);
  arrow3d_head_radius_property_->setHidden(!use_arrow3d);

  axes_length_property_->setHidden(!use_axes);
  axes_radius_property_->setHidden(!use_axes);
}

void ViewpointDisplay::updateShapeChoice() {
  updateProperties();

  if (initialized()) {
    updateDisplay();
  }
}


void ViewpointDisplay::updateArrowColor() {
  int shape = shape_property_->getOptionInt();
  Ogre::ColourValue color = arrow_color_property_->getOgreColor();
  color.a = arrow_alpha_property_->getFloat();

  if (shape == rviz::ShapeType::Arrow2d)
  {
    updateArrows2d();
  }
  else if (shape == rviz::ShapeType::Arrow3d)
  {
    for (std::size_t i = 0; i < arrows3d_.size(); ++i)
    {
      arrows3d_[i].setColor(color);
    }
  }
  context_->queueRender();
}

void ViewpointDisplay::updateArrow2dGeometry() {
  updateArrows2d();
  context_->queueRender();
}

void ViewpointDisplay::updateArrow3dGeometry()
{
  for (std::size_t i = 0; i < viewpoints_.size(); ++i) {
    arrows3d_[i].set(arrow3d_shaft_length_property_->getFloat(),
                     arrow3d_shaft_radius_property_->getFloat(),
                     arrow3d_head_length_property_->getFloat(),
                     arrow3d_head_radius_property_->getFloat());
  }
  context_->queueRender();
}

void ViewpointDisplay::updateAxesGeometry() {
  for (std::size_t i = 0; i < viewpoints_.size(); ++i) {
    axes_[i].set(axes_length_property_->getFloat(), axes_radius_property_->getFloat());
  }
  context_->queueRender();
}

} // namespace viewpoint_rviz_plugin

#include <pluginlib/class_list_macros.hpp>
PLUGINLIB_EXPORT_CLASS(viewpoint_rviz_plugin::ViewpointDisplay, rviz::Display)
